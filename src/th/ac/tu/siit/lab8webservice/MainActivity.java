package th.ac.tu.siit.lab8webservice;
import java.io.*;
import java.net.*;
import java.util.*;
import org.json.*;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

public class MainActivity extends ListActivity {
	List<Map<String,String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;
	String current_P = "bangkok";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		File infile = getBaseContext().getFileStreamPath("province.tsv"); if (infile.exists()) {
			try {
			Scanner sc = new Scanner(infile); 
			while(sc.hasNextLine()) {
				
				
				current_P = sc.nextLine();
		
			
			
			}
			sc.close();
			} catch (FileNotFoundException e) {}
			}
		
		
		
		//Don't allow the layout to be rotated based on the orientation of the device
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		list = new ArrayList<Map<String,String>>();
		adapter = new SimpleAdapter(this, list, R.layout.item, 
				new String[] {"name", "value"}, 
				new int[] {R.id.tvName, R.id.tvValue});
		setListAdapter(adapter);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		refresh(3,current_P);
	}
		
		//Check if the device is connected to a network
	public void refresh(int call_min,String province){
		
		try {
			FileOutputStream outfile = openFileOutput("province.tsv",
					MODE_PRIVATE);
			PrintWriter p = new PrintWriter(outfile);
				p.write(province);
			
			p.flush();
			p.close();
			outfile.close();
		} catch (FileNotFoundException e) {
			Toast t = Toast.makeText(this, "Error:␣Unable␣to␣save␣data",
					Toast.LENGTH_SHORT);
			t.show();
		} catch (IOException e) {
			Toast t = Toast.makeText(this, "Error:␣Unable␣to␣save␣data",
					Toast.LENGTH_SHORT);
			t.show();
		}
		
		
		ConnectivityManager mgr = (ConnectivityManager)
				getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo info = mgr.getActiveNetworkInfo();
		if (info != null && info.isConnected()) {
			//The device is connected to a network
			//Load data
			//Before loading , we compare the current time and the last update
			//if it is longer than 5 mins since the last update,we load the new data.
			long current = System.currentTimeMillis();
			if (current - lastUpdate > call_min*60*1000) {
				// We start an AsyncTask for loading data
				
				WeatherTask task = new WeatherTask(this);
				task.execute("http://ict.siit.tu.ac.th/~cholwich/"+province+".json");
				
				
				// "http://ict.siit.tu.ac.th/~cholwich/bangkok.json"
			
			}
			else {
			Toast t = Toast.makeText(this, 
						"Please refresh after 3 minutes.", 
						Toast.LENGTH_LONG);
				t.show();
			}
		}else {
			Toast t = Toast.makeText(this, 
					"No Internet Connectivity on this device. Check your setting", 
					Toast.LENGTH_LONG);
			t.show();
	}
	
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		int id = item.getItemId();
		switch(id){
		case R.id.refresh01:
			refresh(3,current_P);
			break;
			
		case R.id.bangkok1:
			current_P =  "bangkok";
			refresh(0,current_P);
			
			break;
		
		case R.id.nonthaburi1:
			current_P =  "nonthaburi";
			refresh(0,current_P);
			break;
			
		case R.id.pathum_thani1:
			current_P =  "pathumthani";
			refresh(0,current_P);
			break;
		
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}


	class WeatherTask extends AsyncTask<String, Void, String> {
		Map<String,String> record;
		ProgressDialog dialog;
		
		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}
		
		@Override
		protected void onPreExecute() { //executed under the UI thread before task starts
			super.onPreExecute();
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}

		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			Toast t = Toast.makeText(getApplicationContext(), 
					result, Toast.LENGTH_LONG);
			t.show();
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis();
			//set the title of the MainActivity
			setTitle(Character.toUpperCase(current_P.charAt(0))+current_P.substring(1, current_P.length())+" Weather");
		}

		//Executed under the Background thread
		
		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				
				//Get the first parameter string, and use it as a URL
				URL url = new URL(params[0]);

				//Create a connection to the URL
				
				HttpURLConnection http = (HttpURLConnection)url.openConnection();
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				//Read data from the web server
				http.setDoInput(true);
				http.connect();
				
				response = http.getResponseCode();
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(http.getInputStream()));
					while((line = in.readLine()) != null) {
						buffer.append(line);
					}
					//Extract values from the obtained data
					JSONObject json = new JSONObject(buffer.toString());
					JSONObject jmain = json.getJSONObject("main");
					list.clear();
					record = new HashMap<String,String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp")-273.0;
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", temp));
					list.add(record);
					
					
					
					//pressure
					record = new HashMap<String,String>();
					record.put("name", "Pressure");
					double pressure = jmain.getDouble("pressure");
					record.put("value", String.format(Locale.getDefault(), "%.0f Pa",pressure));
					list.add(record);
					
					
					//humidity
					record = new HashMap<String,String>();
					record.put("name", "Humidity");
					double humidity = jmain.getDouble("humidity");
					record.put("value", String.format(Locale.getDefault(), "%.0f %%",humidity));
					list.add(record);
					
					
					//temp-min
					record = new HashMap<String,String>();
					record.put("name", "Temp-min");
					double tempmin = jmain.getDouble("temp_min")-273.0;
					record.put("value", String.format(Locale.getDefault(), "%.2f degree celcius",tempmin));
					list.add(record);
					
					//temp-max
					record = new HashMap<String,String>();
					record.put("name", "Temp-max");
					double tempmax = jmain.getDouble("temp_max")-273.0;
					record.put("value", String.format(Locale.getDefault(), "%.2f degree celcius",tempmax));
					list.add(record);
					
					
					//extract "description"
					//"weather" : [ { " description :" "...."}]
					JSONArray jweather = json.getJSONArray("weather");
					JSONObject w0 = jweather.getJSONObject(0);
					String description = w0.getString("description");
					record = new HashMap<String,String>();
					record.put("name", "Description");
					record.put("value", description);
					list.add(record);
					
					
					//wind
					
					JSONObject jwind= json.getJSONObject("wind");


					record = new HashMap<String,String>();
					record.put("name", "Wind");
					double windy = jwind.getDouble("speed");
					record.put("value", String.format(Locale.getDefault(), "%.1f km/hr",windy));
					list.add(record);
					
					

					record = new HashMap<String,String>();
					record.put("name", "Wind deg");
					double deg = jwind.getDouble("deg");
					record.put("value", String.format(Locale.getDefault(), "%.1f",deg));
					list.add(record);
					
					
					return "Finished Loading Weather Data";
				}
				else {
					return "Error "+response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}	
		}
	}

}

